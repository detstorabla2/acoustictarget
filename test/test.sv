class shot_class;
   rand bit[3:0] shots;
   randc bit[1:0] n;
   rand bit[6:0] clock_cycles;
   
endclass

module test;
   bit clk,reset;
   time clk_period=20.8ns;
   logic [3:0] shots;
   logic       timeout,clearirq,irq;

   localparam C_COUNTER_WIDTH=8;

   logic [C_COUNTER_WIDTH-1:0] counter0,counter1,counter2,counter3;
   
   
   shot_class m_shot=new();
   
   // Clock generation
   initial begin
      clk = 0;
      forever begin
	 #(clk_period/2);
	 clk = ~clk;
      end
   end

   initial begin
      shots<='0;
      
      reset<=1;
      repeat(3) @(posedge clk);
      reset<=0;
   end
   
   task pulse(input int index);
      $display("Index:%0d at %0t",index,$time());
      
      @(posedge clk) shots[index] <=1;
      @(posedge clk) shots[index] <=0;
   endtask // pulse

   task two_pulse(input int index1,index2);
      $display("Index:%0d and %0d at %0t",index1,index2,$time());      
      @(posedge clk);
      shots[index1] <=1;
      shots[index2] <=1;
      @(posedge clk);
      shots[index1] <=0;
      shots[index2] <=0;
   endtask // pulse
   
   task fully_separated_shot();
      while(irq) @(posedge clk);
      $display("Starting shot at %0t",$time());	       
      repeat(4) begin
	 assert(m_shot.randomize());
	 $display("Sending response from %0d at %0t",m_shot.n,$time());	 
	 pulse(m_shot.n);
	 repeat(m_shot.clock_cycles) @(posedge clk);	 
      end
   endtask // fully_separated_shot

   task two_at_the_same_time();
      int tmp;
      
      while(irq) @(posedge clk);
      $display("Starting shot at %0t",$time());	       
      assert(m_shot.randomize());
      tmp=m_shot.n;
      assert(m_shot.randomize());      
      $display("Sending response from %0d and %0d at %0t",tmp,m_shot.n,$time());
      two_pulse(tmp,m_shot.n);
      repeat(m_shot.clock_cycles) @(posedge clk);	 
      repeat(2) begin
	 assert(m_shot.randomize());
	 $display("Sending response from %0d at %0t",m_shot.n,$time());	 
	 pulse(m_shot.n);
	 repeat(m_shot.clock_cycles) @(posedge clk);	 
      end
   endtask
   
      
   initial begin
      @(negedge reset);
      repeat(16) begin
	 fully_separated_shot();
	 #(100ns);
      end
      repeat(16) begin
	 two_at_the_same_time();
	 #(100ns);
      end
      
      #(100ns);
      $stop();
   end

   initial begin
      forever begin
	 clearirq<=0;	 
	 @(posedge irq);
	 $display("INFO:Detected IRQ at %0t",$time());
	 $display("INFO: - counter0=%0d",counter0);
	 $display("INFO: - counter1=%0d",counter1);
	 $display("INFO: - counter2=%0d",counter2);
	 $display("INFO: - counter3=%0d",counter3);
	 repeat(1) @(posedge clk);
	 if (timeout)
	   $display("INFO:Detected timeout at %0t",$time());
	 repeat(29) @(posedge clk);
	 clearirq<=1;
	 $display("INFO:Clearing IRQ %0t",$time());	 
	 repeat(1) @(posedge clk);
      end // forever begin
   end // initial begin
   
   
      
   
   at #(.COUNTER_SIZE(C_COUNTER_WIDTH)) dut(
					    .clk(clk),
					    .reset(reset),
					    .shots(shots),
					    .clearirq(clearirq),
					    .irq(irq),
					    .counter0(counter0),
					    .counter1(counter1),
					    .counter2(counter2),
					    .counter3(counter3),
					    .timeoutindication(timeout));  


   bind at coverage_collector#(8) i_coverage_collector(.*);
   

   


endmodule // test
