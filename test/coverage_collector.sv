module coverage_collector#(parameter COUNTER_SIZE)( // Ports to dut
						  input 		   clk,
						  input 		   reset,
						  input [3:0] 		   shots,
						  input 		   clearirq,
						  input 		   irq, 
						  input [COUNTER_SIZE-1:0] counter0,
						  input [COUNTER_SIZE-1:0] counter1,
						  input [COUNTER_SIZE-1:0] counter2,
						  input [COUNTER_SIZE-1:0] counter3,
						  input 		   timeoutindication,
						   // Internal signals that we check
						   input [3:0] shots_locked,
						   input [3:0][COUNTER_SIZE-1:0] counters

 );
   


   // assertions to make sure that everything works.
   // At least one of the counters need to have all '1 when a timeout indication occurs
   property p_timeout();
      @(posedge clk) disable iff(reset)
	$rose(timeoutindication) |-> (counters[0] == '1)|(counters[1] == '1)|(counters[2] == '1)|(counters[3] == '1);      
   endproperty
   
   // this is more of a detection of bad input stimuli
   property p_clearirq;
      @(posedge clk) disable iff(reset)
	$rose(irq) |=> ##[0:$] $rose(clearirq);
   endproperty

   // There should never be a pulse in when all shots_locked are asserted (bad input stimuli)
   property p_shot_while_busy;
      @(posedge clk) disable iff(reset)
	&shots_locked|-> (shots===4'b0);
   endproperty

   
   a_timeout:assert property(p_timeout) else $error("At least one of the internal counters should be all ones");
   a_clearirq:assert property(p_clearirq) else $error("clearirq missing");
   a_shot_while_busy:assert property(p_shot_while_busy) else $error("Bad input stimuli? We got a shot while we were busy");

   // sequences
   covergroup cg_values @ (posedge irq);
      coverpoint counter0 {
	 bins first={0};
	 bins others[16]={ [1:255]};
			   }
      coverpoint counter1 {
			   bins first={0};
			   bins others[16]={ [1:255]};			   
			   }
      coverpoint counter2 {
			   bins first={0};
			   bins others[16]={ [1:255]};			   
			   }
      coverpoint counter3 {
			   bins first={0};
			   bins others[16]={ [1:255]};			   
			   }
   endgroup // cg_values
   
   cg_values m_cg_values;
   initial m_cg_values=new();
   
   
   
endmodule
   
   
   
	
   

   
