set Perspective_Version   2
#
pref::section perspective
set perspective_Name       {qverify}
set perspective_DateTime   {2022-02-08T12:30:58}
set perspective_Directory  {/home/mikaela/git/acoustictarget/sim}
set perspective_USER       {mikaela}
set perspective_VisId      {2021.4}
#
pref::section preference
pref::set -type bool -category General -name ShowExpandWindowButton -value true -hide -description {Each window's title bar will have a +/- button to enter/exit expanded window mode respectively} -label {Show Expand Window Button in Titlebar}
pref::set -type bool -category {Source Browser} -name EnableValueAnnotation -value false -description {Displays signal values below their names if true} -label {Enable Value Annotation}
pref::set -type int -category {Driver Receiver} -name DriverReceiverMax -value 10000 -description {<p style='white-space: pre'>Maximum number of driver/receiver<br>objects to be shown in the window.<br>Note: Change is seen in next session.<br>Note: Use a value of "0" to restore the default.} -label {Maximum displayed drivers / receivers}
pref::set -type bool -category AutoCheck -name directivesEditorGroup -value false -hide -description none -label none
pref::set -type string -category AutoCheck -name formalControlPointsRadixType -value b -hide -description none -label none
pref::set -type bool -category AutoCheck -name dontCaresX -value false -description {<p style='white-space: pre'>By default, waveforms use 0's for the don't care<br>values. This option applies X's instead. The X's<br>propagate using the semantic rules for X propagation.} -label {Use X for don't care values on control points}
pref::set -type bool -category AutoCheck -name runShowsMonitor -value true -description {<p style='white-space: pre'>The Run Monitor window will open<br>when a new verify run begins.} -label {Open Run Monitor when a run begins}
pref::set -type bool -category AutoCheck -name autocheckShowInconclusives -value false -hide -description {<p style='white-space: pre'>Show inconclusive checks<br>} -label {Show inconclusive checks}
pref::set -type bool -category AutoCheck -name autocheckShowSecondary -value false -description {This removes the root analysis filter} -label {Show all checks}
pref::set -type int -category AutoCheck -name extraCycles -value 1 -description {<p style='white-space: pre'>Include in the waveform the specified number<br>of cycles after the cycle with the firing} -label {Number of cycles after firing}
pref::set -type string -category AutoCheck -name waveGroupStates -value grpClks=1grpRsts=2grpPropSigs=1grpCtrlPts=1grpContribs=0 -hide -description none -label none
pref::set -type category -value AutoCheck.Color
pref::set -type color -category AutoCheck.Color -name contribWaveAnnoColor -value #3c5e36 -description {<p style='white-space: pre'>Color used in wave window to identify ranges of times for which<br>a contributing signal's values are relevant for the firing.<br>Note: Change not applied to currently visible waveforms.} -label {Contributor time regions}
pref::set -type category -value AutoCheck.table_columns -hide
pref::set -type string -category AutoCheck.table_columns -name autocheck_Table -value %00%00%00%FF%00%00%00%00%00%00%00%01%00%00%00%01%00%00%00%01%01%00%00%00%00%00%00%00%00%00%00%00%14%01%F8%0F%00%00%00%0A%00%00%00%12%00%00%00%2C%00%00%00%13%00%00%00%2C%00%00%00%10%00%00%00Y%00%00%00%11%00%00%00%2C%00%00%00%00%00%00%00d%00%00%00%0E%00%00%01d%00%00%00%0F%00%00%00%85%00%00%00%0C%00%00%00%B2%00%00%00%0D%00%00%00%B2%00%00%00%0B%00%00%02%15%00%00%0C%5E%00%00%00%14%01%01%00%01%00%00%00%00%00%00%00%00%00%00%00%00d%FF%FF%FF%FF%00%00%00%01%00%00%00%00%00%00%00%14%00%00%00%00%00%00%00%01%00%00%00%00%00%00%00%1B%00%00%00%01%00%00%00%00%00%00%00%B2%00%00%00%01%00%00%00%00%00%00%00%B2%00%00%00%01%00%00%00%00%00%00%00%B2%00%00%00%01%00%00%00%00%00%00%00%B2%00%00%00%01%00%00%00%00%00%00%00%B2%00%00%00%01%00%00%00%00%00%00%01%BD%00%00%00%01%00%00%00%00%00%00%00%85%00%00%00%01%00%00%00%00%00%00%00Y%00%00%00%01%00%00%00%00%00%00%06.%00%00%00%01%00%00%00%00%00%00%00%00%00%00%00%01%00%00%00%00%00%00%00%00%00%00%00%01%00%00%00%00%00%00%00%00%00%00%00%01%00%00%00%00%00%00%00%00%00%00%00%01%00%00%00%00%00%00%00%00%00%00%00%01%00%00%00%00%00%00%00%00%00%00%00%01%00%00%00%00%00%00%00%00%00%00%00%01%00%00%00%02%00%00%00%00%00%00%00%01%00%00%00%02%00%00%00%00%00%00%00%01%00%00%00%02%00%00%03%E8%00%00%00%00Y -description unused -label unused
pref::set -type string -category AutoCheck.table_columns -name autocheck_Table_signature -value SEQ#,,Type,Signal,Details,Status,Module,Instance,ID,Time,Radius,File,Owner,Reviewer,Message,Phase,Line,Inconclusive,Evaluation,SourceKey -description unused -label unused
pref::set -type category -value privateAutoCheck -hide
pref::set -type string -category privateAutoCheck -name AutoCheckSummarySizes -value 280,3182 -hide -description unused -label unused
Perspective_Complete
