#! /bin/bash 
ZI_PLATFORM="" 

if [[ `uname` -eq "Linux" ]] ; then 
  case `uname -m` in 
    x86_64) 
      ZI_PLATFORM="linux_x86_64" 
    ;; 
    aarch64) 
      ZI_PLATFORM="linux_aarch64" 
    ;; 
  esac 
fi 


export QHOME="/home/mikaela/sw/questa_formal/2021.4/${ZI_PLATFORM}"
export HOME_0IN="/home/mikaela/sw/questa_formal/2021.4/${ZI_PLATFORM}"

/home/mikaela/sw/questa_formal/2021.4/${ZI_PLATFORM}/bin/qverifypm --monitor --host localhost --port 43734 --wd /home/mikaela/git/acoustictarget/sim --type master --binary /home/mikaela/sw/questa_formal/2021.4/${ZI_PLATFORM}/bin/qverifyfk --id 0 -tool autocheck -import_db /home/mikaela/git/acoustictarget/sim/autocheck_logs/autocheck.db -od . -hidden_dir ./.qverify -netcache /tmp/propcheck.115951_591 -gui -pm_host localhost -pm_port 43734   
