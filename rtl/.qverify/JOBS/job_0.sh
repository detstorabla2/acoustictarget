#! /bin/bash 
ZI_PLATFORM="" 

if [[ `uname` -eq "Linux" ]] ; then 
  case `uname -m` in 
    x86_64) 
      ZI_PLATFORM="linux_x86_64" 
    ;; 
    aarch64) 
      ZI_PLATFORM="linux_aarch64" 
    ;; 
  esac 
fi 


export QHOME="/home/mikaela/sw/questa_formal/2021.4/${ZI_PLATFORM}"
export HOME_0IN="/home/mikaela/sw/questa_formal/2021.4/${ZI_PLATFORM}"

/home/mikaela/sw/questa_formal/2021.4/${ZI_PLATFORM}/bin/qverifypm --monitor --host localhost --port 41071 --wd /home/mikaela/git/acoustictarget/rtl --type master --binary /home/mikaela/sw/questa_formal/2021.4/${ZI_PLATFORM}/bin/qverifyfk --id 0 -netcache /home/mikaela/git/acoustictarget/rtl/autocheck.db/AUTOCHECK/NET -tool autocheck -hd .qverify -import_db ./autocheck.db/autocheck_compile.db -zdb /home/mikaela/git/acoustictarget/rtl/autocheck.db/DB/zdb_18 -pm_host localhost -pm_port 41071   
