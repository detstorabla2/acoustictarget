module at #(COUNTER_SIZE=8)(input clk,
			    input 			    reset,
			    input [3:0] 		    shots,
			    input 			    clearirq,
			    output logic 		    irq, 
			    output logic [COUNTER_SIZE-1:0] counter0,
			    output logic [COUNTER_SIZE-1:0] counter1,
			    output logic [COUNTER_SIZE-1:0] counter2,
			    output logic [COUNTER_SIZE-1:0] counter3,
			    output logic 		    timeoutindication);

   import at_pkg::*;

   logic [1:0] first_shot;
   logic [3:0] shots_d,shots_2d,shots_locked;
   logic       counter_timeout;

   state_t state;

   logic [3:0][COUNTER_SIZE-1:0] counters;

   // Help function for detecting first which channel we got the first pulse on
   function bit[1:0] get_first_shot(input logic[3:0]  sh);
      for (int i=0;i<4;i++) begin		 
	 if (sh[i]) 
	   return i;
      end
      return 0;
   endfunction // get_first_shot


   // To avoid metastability problems from asynchronous signals
   always_ff @(posedge clk  or posedge reset) begin
           if (reset) begin	
	      shots_d <= '0;
	      shots_2d <= '0;      
	   end else begin	
	      shots_d <= shots;
	      shots_2d <= shots_d;
	   end      
   end

   // lock the signals until we got all pulses
   always_ff @(posedge clk or posedge reset)
     if (reset) begin
	 shots_locked <='0;	
      end else 
	begin
	   if ((state  != got_all_shots)&(state!=timeout))
	     for (int i=0;i<4;i++) 	       
	       shots_locked[i] <= ~shots_locked[i]?shots_2d[i]:shots_locked[i]; // Lock the signal
	   else 
	     shots_locked <='0;	   
	end

   
   // Detect timeout = if any counter is all ones we have got a timeout   
   always_ff @(posedge clk or posedge reset) begin
      if (reset) begin
	 counter_timeout<='0;
      end
      else begin
	 counter_timeout<=(counters[0] == '1)|(counters[1] == '1)|(counters[2] == '1)|(counters[3] == '1);
	 
      end
   end
   
   
   // Lets build a state machine
   always_ff @(posedge clk or posedge reset) begin
      if (reset) begin
	 state<= idle;
	 first_shot <='0;
	 irq <= '0;
	 timeoutindication <='0;	 
      end else begin

	 case (state) 
	    idle: 
	      if (|shots_locked) begin // if any or all 
		 first_shot <= get_first_shot(shots_locked);
		 state <=got_first_shot;
	      end

	    got_first_shot: 
	      if (&shots_locked) begin
		state <= got_all_shots;
	      end
	      else if (counter_timeout) begin
		state <=timeout;
		 timeoutindication <='1;
	      end
	   
	    got_all_shots: begin
	       irq <='1;
	       if (clearirq) begin
		  state <= idle; // Stay here until interrupt is serviced
		  irq <='0;
	       end	       
	    end
	   
	    timeout:begin
	       irq <='1;	       
	       if (clearirq) begin
		  state <= idle; // Stay here until interrupt is serviced
	          timeoutindication <='0;
		  irq <='0;		  
	       end	       
	    end
	   default:state <= idle;	   
	 endcase // case (state)
      end
   end // always_ff @ (posedge clk or posedge reset)


   // catch the counter values (might not be necessary)
   
   always_ff @(posedge clk or posedge reset ) begin
      if (reset) begin
	 counter0<='0;
	 counter1<='0;	 
	 counter2<='0;	 
	 counter3<='0;
      end
      else begin
	 if (&shots_locked) begin
	    counter0<=counters[0];
	    counter1<=counters[1];
	    counter2<=counters[2];
	    counter3<=counters[3];
	 end
      end      
   end // always_ff @ (posedge clk or posedge reset )
   

   
   // Instantiate 4 Counters
   genvar n;
   generate
      for (n=0;n<4;n++) begin: delaycounter
	 at_counter #(.COUNTER_SIZE(COUNTER_SIZE),
		      .index(n)) i_counter(.locked_shot(shots_locked[n]),
					   .counter(counters[n]),
					   .*);	 
      end
   endgenerate
   
endmodule // at
