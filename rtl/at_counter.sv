

import at_pkg::*;
module at_counter #(parameter COUNTER_SIZE=8,parameter index=0) (input clk,
							   input 			   reset,
							   input 			   state_t state,
							   input [1:0] 			   first_shot,
							   input 			   locked_shot,
							   output logic [COUNTER_SIZE-1:0] counter);

   always_ff @(posedge clk or posedge reset)
     if (reset)
       counter <= '0;
     else begin
	if (state == got_first_shot) begin
	   if (~locked_shot & first_shot!=index)
 	         counter <= (counter=='1)? counter:counter +1;
	     end
	else
	  if (state == idle) begin
	     counter <= '0;
	  end
	      
     end

endmodule // at_counter
